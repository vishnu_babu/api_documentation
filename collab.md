**INVITE USERS**
----

* **URL**
  /collab.invite
* **METHOD**
  POST


**CREATE BOARD**
----

* **URL**
  /collab/boards
* **METHOD**
  POST

**GET ALL BOARDS**
----

* **URL**
  /collab/boards
* **METHOD**
  GET


**GET A SINGLE BOARD**
----

* **URL**
  /collab/boards/{board-id}
* **METHOD**
  GET


**UPDATE BOARD**
----

* **URL**
  /collab/boards/{board-id}
* **METHOD**
  PUT


**DELETE BOARD**
----

* **URL**
  /collab/boards/{board-id}
* **METHOD**
  DELETE


**CREATE A PRODUCT FOLDER**
----

* **URL**
  /collab/boards/{board-id}/products
* **METHOD**
  POST


**GET ALL PRODUCT FOLDERS IN A BOARD**
----

* **URL**
  /collab/boards/{board-id}/products
* **METHOD**
  GET


**GET A PRODUCT FOLDERS IN A BOARD**
----

* **URL**
  /collab/boards/{board-id}/products/{product-id}
* **METHOD**
  GET

**DELETE A PRODUCT FOLDER**
----

* **URL**
  /collab/boards/{board-id}/products/{products-id}
* **METHOD**
  DELETE


**ARCHIVE A PRODUCT FOLDER**
----

* **URL**
  /collab/boards/{board-id}/products/{products-id}?archive=true
* **METHOD**
  DELETE


**ADD PICK TO PRODUCT FOLDER**
----

* **URL**
  /collab/products/{products-id}/picks
* **METHOD**
  POST


**GET ALL PICKS IN A PRODUCT FOLDER**
----

* **URL**
  /collab/products/{product-id}/picks
* **METHOD**
  GET


**GET A PICK IN A PRODUCT FOLDER**
----

* **URL**
  /collab/products/{product-id}/picks/{pick-id}
* **METHOD**
  GET


**UPDATE A PICK IN A PRODUCT FOLDER**
----

* **URL**
  /collab/products/{product-id}/picks/{pick-id}
* **METHOD**
  PUT


**DELETE A PICK IN A PRODUCT FOLDER**
----

* **URL**
  /collab/products/{product-id}/picks/{pick-id}
* **METHOD**
  DELETE
  

**LIKE A PICK**
----

* **URL**
  /collab/picks/{pick-id}/likes/{user-id}
* **METHOD**
  PUT


**REMOVE LIKE FROM A PICK**
----

* **URL**
  /collab/picks/{pick-id}/likes/{user-id}
* **METHOD**
  DELETE


**COMMENT ON A PICK**
----

* **URL**
  /collab/picks/{pick-id}/comments
* **METHOD**
  POST


**REMOVE A COMMENT FROM A PICK**
----

* **URL**
  /collab/picks/{pick-id}/comments/{comment-id}
* **METHOD**
  DELETE
